import javax.imageio.*;
import javax.imageio.stream.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * This is a personal extension of JPanel. I just did this so that the panel includes an image
 *
 * @author Sam
 */
public class ImageJPanel extends JPanel {

  static final long serialVersionUID = 201606281539L;

  //The image to include
  public BufferedImage image;

  //Constructor
  ImageJPanel(boolean b, BufferedImage img) {
    super(b);
    image = img;
  }

  //The painting of the panel should also redraw the image
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(image, 0, 0, null);
  }
}
