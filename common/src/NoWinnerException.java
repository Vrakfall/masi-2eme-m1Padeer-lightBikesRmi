/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class NoWinnerException extends Exception {
  //==== Static variables ====//

  public static final String DEFAULT_MESSAGE = "There's no winner for this game at the moment.";

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NoWinnerException(int playersNumber) {
    super(DEFAULT_MESSAGE + " There are " + playersNumber + " players still in the game.");
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
