import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public interface GameRemote extends Remote {
  void startGame() throws RemoteException;
  void receiveNewDirection(String playerName, char newDirection) throws RemoteException;
}
