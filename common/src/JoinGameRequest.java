import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class JoinGameRequest implements Serializable {
  //==== Static variables ====//

  //==== Attributes ====//

  private PlayerWithClient player;
  private String gameName;
  private UUID uuid;

  //==== Getters and Setters ====//

  public PlayerWithClient getPlayer() {
    return player;
  }

  public String getGameName() {
    return gameName;
  }

  public UUID getUuid() {
    return uuid;
  }

  //==== Constructors ====//

  public JoinGameRequest(PlayerWithClient player, String gameName, UUID uuid) {
    this.player = player;
    this.gameName = gameName;
    this.uuid = uuid;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
