import java.io.Serializable;

/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class GameListElement implements Serializable {
  //==== Static variables ====//

  //==== Attributes ====//
  private String name;
  private int numberOfPlayers;

  //==== Getters and Setters ====//

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNumberOfPlayers() {
    return numberOfPlayers;
  }

  public void setNumberOfPlayers(int numberOfPlayers) {
    this.numberOfPlayers = numberOfPlayers;
  }

  //==== Constructors ====//

  public GameListElement(String name, int numberOfPlayers) {
    this.name = name;
    this.numberOfPlayers = numberOfPlayers;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
