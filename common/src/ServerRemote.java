import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
interface ServerRemote extends Remote {
  String SERVER_ID = "lightBikesRmiServer";
  String SERVER_IP = "127.0.0.1";
  int SERVER_PORT = 6444;

  UUID connect(String nickname, ClientRemote clientRemote) throws RemoteException;
  GameListElement[] getListOfGames(UUID uuid) throws RemoteException;
//  GameRemote connectAndCreateWaitingGame(NewGameRequest newGameRequest) throws RemoteException;
//  GameRemote connectAndJoinGame(JoinGameRequest joinGameRequest) throws RemoteException;
  GameRemote joinGame(JoinGameRequest joinGameRequest) throws RemoteException;
  GameRemote createWaitingGame(NewGameRequest newGameRequest) throws RemoteException;
}
