import java.awt.*;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class PlayerWithClient extends Player {
  //==== Static variables ====//

  //==== Attributes ====//

  private ClientRemote clientRemote;

  //==== Getters and Setters ====//

  public ClientRemote getClientRemote() {
    return clientRemote;
  }

  //==== Constructors ====//

  public PlayerWithClient(String nickname, Color color, int score, int xPosition, int yPosition, boolean isHuman, char carDirection, boolean isAlive, ClientRemote clientRemote) {
    super(nickname, color, score, xPosition, yPosition, isHuman, carDirection, isAlive);
    this.clientRemote = clientRemote;
  }

  public PlayerWithClient(String nickname, Color color, int xPosition, int yPosition, boolean isHuman, char carDirection, ClientRemote clientRemote) {
    super(nickname, color, xPosition, yPosition, isHuman, carDirection);
    this.clientRemote = clientRemote;
  }

  public PlayerWithClient(Player player, ClientRemote clientRemote) {
    super(player);
    this.clientRemote = clientRemote;
  }

  public PlayerWithClient(PlayerWithClient playerWithClient) {
    super(playerWithClient.getNickname(),
        playerWithClient.getColor(),
        playerWithClient.getScore(),
        playerWithClient.getPositionX(),
        playerWithClient.getPositionY(),
        playerWithClient.isHuman(),
        playerWithClient.getCarDirection(),
        playerWithClient.isAlive());
    this.clientRemote = playerWithClient.getClientRemote();
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
