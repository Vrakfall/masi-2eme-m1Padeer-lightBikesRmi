import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jérémy Lecocq.
 * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
 * and Chat RMI August exams (Jérémy Lecocq).
 *
 * @author Jérémy Lecocq
 */
class IOTools implements IORegularExpressions {
  //====== Static Methods ======//
  //======== Public ========//

  static String getRegexCapturedString(String patternStringToMatch, String stringToCompare, int captureGroupNumber) {
    //Creates the pattern and matcher so we can compare them.
    Pattern patternToMatch = Pattern.compile(patternStringToMatch);
    Matcher matcher = patternToMatch.matcher(stringToCompare);

    if (matcher.matches()) {
//      System.out.println("Regex going to be returned: " + matcher.group(captureGroupNumber));
      return matcher.group(captureGroupNumber);
    }

//    System.out.println("Regex's output is null!");
    return null;
  }

  static String parseUserName(String stringToParse) {
    return getRegexCapturedString(USERNAME, stringToParse, USERNAME_GROUP);
  }

  static String parseIpAddress(String stringToParse) {
    return getRegexCapturedString(IP_ADDRESS, stringToParse, IP_ADDRESS_GROUP);
  }

  static String parseHostname(String stringToParse) {
    return getRegexCapturedString(HOSTNAME, stringToParse, HOSTNAME_GROUP);
  }

  static String parseIpAddressOrGetHostname(String stringToParse) {
    String stringToReturn = parseIpAddress(stringToParse);

    if (stringToReturn == null) {
      return parseHostname(stringToParse);
    }

    return stringToReturn;
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
