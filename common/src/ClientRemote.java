import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
interface ClientRemote extends Remote {
  void startGame(GameState gameState) throws RemoteException;

  void update(GameState gameState) throws RemoteException;
}
