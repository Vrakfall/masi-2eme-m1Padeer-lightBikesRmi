import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class NewGameRequest implements Serializable {
  //==== Static variables ====//

  //==== Attributes ====//

  private PlayerWithClient creatorPlayer;
  private String gameName;
//  private int numberPlayers;
  private UUID uuid;

  //==== Getters and Setters ====//

  public PlayerWithClient getCreatorPlayer() {
    return creatorPlayer;
  }

  public String getGameName() {
    return gameName;
  }

//  public int getNumberPlayers() {
//    return numberPlayers;
//  }

  public UUID getUuid() {
    return uuid;
  }

  //==== Constructors ====//

  public NewGameRequest(PlayerWithClient creatorPlayer, String gameName, UUID uuid) {
    this.creatorPlayer = creatorPlayer;
    this.gameName = gameName;
//    this.numberPlayers = numberPlayers;
    this.uuid = uuid;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
