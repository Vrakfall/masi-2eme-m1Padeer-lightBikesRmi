import java.awt.*;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class InnerPlayerWithClient extends PlayerWithClient {
  //==== Static variables ====//

  //==== Attributes ====//

  private char oldDirection;
  private UUID uuid;

  //==== Getters and Setters ====//

  @Override
  public void setCarDirection(char carDirection) {
    if (isNewDirectionAllowed(carDirection)) {
      super.setCarDirection(carDirection);
      oldDirection = carDirection;
    }
  }

  public UUID getUuid() {
    return uuid;
  }
  //==== Constructors ====//

  public InnerPlayerWithClient(PlayerWithClient playerWithClient, UUID uuid) {
    super(playerWithClient);
    this.oldDirection = getCarDirection();
    this.uuid = uuid;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  private boolean isNewDirectionAllowed(char newDirection) {
    switch (newDirection) {
      case 'L':
        return oldDirection != 'R';
      case 'R':
        return oldDirection != 'L';
      case 'U':
        return oldDirection != 'D';
      case 'D':
        return oldDirection != 'U';
      default:
        return false;
    }
  }

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
