import java.awt.*;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 * <p>
 * This is the core class. This class is responsible for keeping the state of the game as well
 * as asking each AI for its input.
 *
 * @author Sam, Jérémy Lecocq
 */
public class Game implements CommonParameters, Serializable, Runnable, GameRemote {
  //==== Static variables ====//

  //==== Attributes ====//

  private String name;

  /**
   * True if the GUI is closing. False otherwise
   */
  private boolean isQuitCalled;
  private boolean isGameStarted;

  private boolean isWaiting;
  private int numberPlayers;
  private CrudHashMapSingleValues<String, Player> players;
  //  private CrudHashMapSingleValues<Color, Player> playersByColor;
  private CrudHashMapSingleValues<String, AI> aIs;
  private CrudHashMapSingleValues<String, InnerPlayerWithClient> humanPlayers;

  private int runTime = 0; //Number of "game ticks"
  private int gameRunTime = 0; //Unused. Used to make a sync every X game ticks
  private int gameMaxRunTime = 1; //Ibid.
  public boolean bGameQuit = false; //Is the program about to be closed
  public String sWinnerName = "NOBODY"; //Current winner of the game

  private Color[][] iGrid = new Color[100][100];                  //Inner grid representation (0 = empty, any = player id)
  private Integer[][] iTimer = new Integer[100][100];                  //Memorizes the "freshness" of paths, for AI to use.

  private Server server;

  //==== Getters and Setters ====//

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isWaiting() {
    return isWaiting;
  }

  public void setWaiting(boolean waiting) {
    isWaiting = waiting;
  }

  //==== Constructors ====//

  public Game(NewGameRequest newGameRequest, Server server) {
    //Initialization
    bGameQuit = false;
    isGameStarted = false;

    this.server = server;
    this.name = newGameRequest.getGameName();

    isQuitCalled = false;

    isWaiting = true;

    players = new CrudHashMapSingleValues<>();
    humanPlayers = new CrudHashMapSingleValues<>();
//    playersByColor = new CrudHashMapSingleValues<>();
    aIs = new CrudHashMapSingleValues<>();

    // Generate a new grid
    initializeBoard();

    addPlayerWithClient(new InnerPlayerWithClient(newGameRequest.getCreatorPlayer(), newGameRequest.getUuid()));

    // For test purposes, at first.
//    addDefaultAIs();

    try {
      UnicastRemoteObject.exportObject(this, 0);
    } catch (RemoteException e) {
      e.printStackTrace();
    }
  }

  //==== Lists' CRUDs ====//

  private synchronized void addPlayerToGeneralLists(Player player) {
    players.addWithoutReplaceNorNullNorEmptyString(player.getNickname(), player);
//    playersByColor.addWithoutReplaceNorNullNorEmptyString(player.getColor(), player);

    setWallAt(player.getPositionX(), player.getPositionY(), player.getColor());
    resetTimerAt(player.getPositionX(), player.getPositionY());
  }

  public synchronized void addPlayerWithClient(InnerPlayerWithClient player) {
    humanPlayers.addWithoutReplaceNorNullNorEmptyString(player.getNickname(), player);
    addPlayerToGeneralLists(player);
  }

  public synchronized void addAI(AI ai) {
    aIs.addWithoutReplaceNorNullNorEmptyString(ai.getNickname(), ai);
    addPlayerToGeneralLists(ai);
  }

  //==== Usual Methods ====//

  /**
   * main method called by the handling thread
   */
  public void run() {
//    aiOpponents[0] = new EasyAI();   //Won't be used, since player 0 is human
//    aiOpponents[1] = new EasyAI();
//    aiOpponents[2] = new MediumAI();
//    aiOpponents[3] = new HardAI();

    if (players.size() < 1) {
      isGameStarted = false;
      return;
    }

    server.switchGameToStarted(name);
    initializeSpawnsAndColors();

    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      playerEntry.getValue().setAlive(true);
    }

    notifyClientsGameIsStarted();

    //While the program hasn't been requested to quit
    while (!bGameQuit) {
      //Increase a game tick (one tick = 50ms; game plays at about 20fps)
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
        //TODO This shouldn't happen but still we should handle it at some point.
      }
      runTime++;
      gameRunTime++;

      //If any player is still "alive"
      if (getNumberOfPlayersAlive() > 0) {
        //Update the freshness of paths (0 = quite old path)
        for (int i = 0; i < 100; i++) {
          for (int j = 0; j < 100; j++) {
            if (iTimer[i][j] > 0)
              iTimer[i][j]--;
          }
        }

        updatePlayersPosition();
      }

      incrementAllScores();
      checkIfGameIsOver();
      sendUpdatedBoardToClients();
    }
    server.removeGame(name);
  }

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //Returns the representation of the grid
  public Color[][] getGrid() {
    return iGrid;
  }

  @Override
  public void startGame() throws RemoteException {
    if (!isGameStarted) {
      isGameStarted = true;
      new Thread(this).start();
    }
  }

  @Override
  public void receiveNewDirection(String playerName, char newDirection) throws RemoteException {
    if (getNumberOfPlayersAlive()>1) {
      humanPlayers.get(playerName).setCarDirection(newDirection);
    }
  }

  public int getNumberOfPlayers() {
    return players.size();
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  /**
   * Create a new and blank grid.
   */
  private void initializeBoard() {
    gameRunTime = 0;
    gameMaxRunTime = 1;
    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        iGrid[i][j] = Color.black;
        iTimer[i][j] = 0;
      }
    }
  }

  /**
   * @return The number of players still alive.
   */
  private int getNumberOfPlayersAlive() {
    int numberPlayersAlive = 0;
    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      if (playerEntry.getValue().isAlive()) {
        numberPlayersAlive++;
      }
    }
    return numberPlayersAlive;
  }

  /**
   * Update the score of the player if it's been 1 second (20 fps) since the last update
   */
  private void incrementAllScores() {
    if (runTime >= 20) {
      runTime = 0;

      for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
        Player player = playerEntry.getValue();
        if (player.isAlive()) {
          player.incrementScore();
        }
      }
    }
  }

  /**
   * Add the default AIs to the game. Supposedly for test purposes only.
   */
  private void addDefaultAIs() {
    addAI(new EasyAI(AI_01_DEFAULT_NAME, AI_01_DEFAULT_COLOR, AI_01_DEFAULT_STARTING_X_POSITION, AI_01_DEFAULT_STARTING_Y_POSITION, false, AI_01_DEFAULT_STARTING_CAR_DIRECTION));
    addAI(new MediumAI(AI_02_DEFAULT_NAME, AI_02_DEFAULT_COLOR, AI_02_DEFAULT_STARTING_X_POSITION, AI_02_DEFAULT_STARTING_Y_POSITION, false, AI_02_DEFAULT_STARTING_CAR_DIRECTION));
    addAI(new HardAI(AI_03_DEFAULT_NAME, AI_03_DEFAULT_COLOR, AI_03_DEFAULT_STARTING_X_POSITION, AI_03_DEFAULT_STARTING_Y_POSITION, false, AI_03_DEFAULT_STARTING_CAR_DIRECTION));
  }

  /**
   * Reset the timer at the given position. Used for grid initialization.
   *
   * @param xPosition The position's X value.
   * @param yPosition The position's Y value.
   */
  private void resetTimerAt(int xPosition, int yPosition) {
    iTimer[xPosition][yPosition] = 10;
  }

  /**
   * Set a wall at the given position.
   *
   * @param xPosition The position's X value.
   * @param yPosition The position's Y value.
   * @param color
   */
  private void setWallAt(int xPosition, int yPosition, Color color) {
    iGrid[xPosition][yPosition] = color;
  }

  /**
   * Update the position of each player.
   */
  private void updatePlayersPosition() {
    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      Player player = playerEntry.getValue();

      if (player.isAlive()) {
        if (player instanceof AI) {
          ((AI) player).setNewDir(iGrid, iTimer);
        }

        player.updatePostion(iGrid);

        iGrid[player.getPositionX()][player.getPositionY()] = player.getColor();
        resetTimerAt(player.getPositionX(), player.getPositionY());
      }
    }
  }

  /**
   * If "highlander" (there's only one left), this one wins, and the current game ends.
   */
  private void checkIfGameIsOver() {
    if (getNumberOfPlayersAlive() <= 1) {
      for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
        if (playerEntry.getValue().isAlive()) {
          sWinnerName = playerEntry.getValue().getNickname();
          playerEntry.getValue().setAlive(false);
          playerEntry.getValue().setWinner(true);
        }
      }
      bGameQuit = true;
    }
  }

  private void notifyClientsGameIsStarted() {
    CrudHashMapSingleValues<String, Player> playersToBeSent = getConvertedShallowCopyOfPlayers();
    ArrayList<InnerPlayerWithClient> playersToRemove = new ArrayList<>();

    for (Map.Entry<String, InnerPlayerWithClient> humanPlayerEntry : humanPlayers.entrySet()) {
      InnerPlayerWithClient connectedPlayer = humanPlayerEntry.getValue();
      try {
        connectedPlayer.getClientRemote().startGame(new GameState(playersToBeSent, iGrid, iTimer));
        //TODO Make so the Game class handles the game variables through a GameState object.
      } catch (RemoteException e) {
        playersToRemove.add(connectedPlayer);
      }
    }

    for (InnerPlayerWithClient connectedPlayer : playersToRemove) {
      removePlayer(connectedPlayer);
    }
  }

  private void removePlayer(InnerPlayerWithClient connectedPlayer) {
    System.out.println("Dropped the client " + connectedPlayer.getNickname());
    server.removePlayer(connectedPlayer.getUuid());
    players.remove(connectedPlayer.getNickname());
    humanPlayers.remove(connectedPlayer.getNickname());
    aIs.remove(connectedPlayer.getNickname());
  }

  private void sendUpdatedBoardToClients() {
    CrudHashMapSingleValues<String, Player> playersToBeSent = getConvertedShallowCopyOfPlayers();
    ArrayList<InnerPlayerWithClient> playersToRemove = new ArrayList<>();

    for (Map.Entry<String, InnerPlayerWithClient> humanPlayerEntry : humanPlayers.entrySet()) {
      InnerPlayerWithClient connectedPlayer = humanPlayerEntry.getValue();
      try {
        connectedPlayer.getClientRemote().update(new GameState(playersToBeSent, iGrid, iTimer));
        //TODO Make so the Game class handles the game variables through a GameState object.
      } catch (RemoteException e) {
        playersToRemove.add(connectedPlayer);
      }
    }

    for (InnerPlayerWithClient connectedPlayer : playersToRemove) {
      removePlayer(connectedPlayer);
    }
  }

  /**
   * Convert every player to the Player class so it can be sent through RMI and clients can't acces other clients' controller object.
   * @return
   */
  private CrudHashMapSingleValues<String, Player> getConvertedShallowCopyOfPlayers() {
    CrudHashMapSingleValues<String, Player> convertedPlayers = new CrudHashMapSingleValues<>();

    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      Player player = playerEntry.getValue();
      convertedPlayers.put(player.getNickname(), new Player(player));
    }
    return convertedPlayers;
  }

  private void initializeSpawnsAndColors() {
    int i = 0;

    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      Player player = playerEntry.getValue();

      switch (i) {
        case 0:
          player.setPositionX(PLAYER_DEFAULT_STARTING_X_POSITION);
          player.setPositionY(PLAYER_DEFAULT_STARTING_Y_POSITION);
          player.setCarDirection(PLAYER_DEFAULT_STARTING_CAR_DIRECTION);
          player.setColor(POSITION_01_DEFAULT_COLOR);
          break;
        case 1:
          player.setPositionX(AI_01_DEFAULT_STARTING_X_POSITION);
          player.setPositionY(AI_01_DEFAULT_STARTING_Y_POSITION);
          player.setCarDirection(AI_01_DEFAULT_STARTING_CAR_DIRECTION);
          player.setColor(POSITION_02_DEFAULT_COLOR);
          break;
        case 2:
          player.setPositionX(AI_02_DEFAULT_STARTING_X_POSITION);
          player.setPositionY(AI_02_DEFAULT_STARTING_Y_POSITION);
          player.setCarDirection(AI_02_DEFAULT_STARTING_CAR_DIRECTION);
          player.setColor(POSITION_03_DEFAULT_COLOR);
          break;
        case 3:
          player.setPositionX(AI_03_DEFAULT_STARTING_X_POSITION);
          player.setPositionY(AI_03_DEFAULT_STARTING_Y_POSITION);
          player.setCarDirection(AI_03_DEFAULT_STARTING_CAR_DIRECTION);
          player.setColor(POSITION_04_DEFAULT_COLOR);
          break;
      }
      ++i;
    }

    for (;i<4;++i) {
      switch (i) {
        case 1:
          addAI(new EasyAI(AI_01_DEFAULT_NAME, POSITION_02_DEFAULT_COLOR, AI_01_DEFAULT_STARTING_X_POSITION, AI_01_DEFAULT_STARTING_Y_POSITION, false, AI_01_DEFAULT_STARTING_CAR_DIRECTION));
          break;
        case 2:
          addAI(new MediumAI(AI_02_DEFAULT_NAME, POSITION_03_DEFAULT_COLOR, AI_02_DEFAULT_STARTING_X_POSITION, AI_02_DEFAULT_STARTING_Y_POSITION, false, AI_02_DEFAULT_STARTING_CAR_DIRECTION));
          break;
        case 3:
          addAI(new EasyAI(AI_03_DEFAULT_NAME, POSITION_04_DEFAULT_COLOR, AI_03_DEFAULT_STARTING_X_POSITION, AI_03_DEFAULT_STARTING_Y_POSITION, false, AI_03_DEFAULT_STARTING_CAR_DIRECTION));
          break;
      }
    }
  }

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //==== Others ====//
}
