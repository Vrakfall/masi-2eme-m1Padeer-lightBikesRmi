import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Amaury on 25/11/2016.
 */
public interface GameClient extends Remote {
    String connect(String s) throws RemoteException ;/*connect to the serevr **/
    ArrayList getSaloon(String uid) throws RemoteException;/**/
    boolean createSaloon(String s,String uid) throws RemoteException;/**/
    void disconnect(String uidd) throws RemoteException;/**/
    void joinGame(String game,String uid);/*Join teh game*/
}