import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class ClientIdentifier {
  //==== Static variables ====//

  //==== Attributes ====//
  private UUID uuid;
  private String nickname;
  private ClientRemote clientRemote;

  //==== Getters and Setters ====//

  public UUID getUuid() {
    return uuid;
  }

  public String getNickname() {
    return nickname;
  }

  public ClientRemote getClientRemote() {
    return clientRemote;
  }

  //==== Constructors ====//

  public ClientIdentifier(String nickname, ClientRemote clientRemote) {
    this.uuid = UUID.randomUUID();
    this.nickname = nickname;
    this.clientRemote = clientRemote;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
