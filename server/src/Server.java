import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class Server extends UnicastRemoteObject implements Serializable, ServerRemote {
  //==== Static variables ====//

  private Registry registry;
  private CrudHashMapSingleValues<String, Game> games;
  private CrudHashMapSingleValues<String, Game> waitingGames;
  private CrudHashMapSingleValues<String, Game> playingGames;
  private CrudHashMapSingleValues<UUID, ClientIdentifier> connectedClients;

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//
  public Server() throws RemoteException, AlreadyBoundException {
    super();
    games = new CrudHashMapSingleValues<>();
    waitingGames = new CrudHashMapSingleValues<>();
    playingGames = new CrudHashMapSingleValues<>();
    connectedClients = new CrudHashMapSingleValues<>();

    this.registry = LocateRegistry.createRegistry(Server.SERVER_PORT);

    registry.bind(Server.SERVER_ID, this);

    System.out.println("Server is started");
  }

  //==== Methods ====//

  private boolean addGame(Game game) {
    return games.addWithoutReplaceNorNullNorEmptyString(game.getName(), game)
        && waitingGames.addWithoutReplaceNorNullNorEmptyString(game.getName(), game);
  }

//  @Override
//  public GameRemote connectAndCreateWaitingGame(NewGameRequest newGameRequest) throws RemoteException {
//    Game game = new Game(newGameRequest);
//    addGame(game);
//    return game;
//  }

  @Override
  public GameRemote createWaitingGame(NewGameRequest newGameRequest) throws RemoteException {
    if (!uuidIsConnected(newGameRequest.getUuid())) {
      return null;
    }

    Game game = new Game(newGameRequest, this);
    addGame(game);
    return game;
  }

  @Override
  public GameRemote joinGame(JoinGameRequest joinGameRequest) throws RemoteException {
    if (!uuidIsConnected(joinGameRequest.getUuid())) {
      return null;
    }

    Game game = games.get(joinGameRequest.getGameName());

    if (game == null) {
      return null;
    }

    game.addPlayerWithClient(new InnerPlayerWithClient(joinGameRequest.getPlayer(), joinGameRequest.getUuid()));
    return game;
  }

  @Override
  public UUID connect(String nickname, ClientRemote clientRemote) throws RemoteException {
    ClientIdentifier clientIdentifier = new ClientIdentifier(nickname, clientRemote);
    if (!connectedClients.addWithoutReplaceNorNullNorEmptyString(clientIdentifier.getUuid(), clientIdentifier)) {
      // this shouldn't happen. However, if it does, let's just recall the method and generate a new and random uuid.
      return connect(nickname, clientRemote);
      //TODO Implement a check so it doesn't do too many recursive calls.........
    }
    return clientIdentifier.getUuid();
  }

  @Override
  public GameListElement[] getListOfGames(UUID uuid) throws RemoteException {
    if (!uuidIsConnected(uuid) || waitingGames.size() <= 0) {
      return new GameListElement[0];
    }

    ArrayList<GameListElement> gameList = new ArrayList<>(waitingGames.size()); //The size can change but we preset it for performance optimization.

    for (Map.Entry<String, Game> gameEntry : waitingGames.entrySet()) {
      Game game = gameEntry.getValue();
      gameList.add(new GameListElement(game.getName(), game.getNumberOfPlayers()));
    }

    return gameList.toArray(new GameListElement[gameList.size()]);
  }

  private boolean uuidIsConnected(UUID uuid) {
    for (Map.Entry<UUID, ClientIdentifier> clientIdentifierEntry : connectedClients.entrySet()) {
      if (clientIdentifierEntry.getValue().getUuid().equals(uuid)) {
        return true;
      }
    }
    return false;
  }


  public void switchGameToStarted(String gameName) {
    playingGames.addWithoutReplaceNorNullNorEmptyString(gameName, waitingGames.get(gameName));
    waitingGames.remove(gameName);
  }

  public void removeGame(String gameName) {
    System.out.println("Game dropped " + gameName);
    games.remove(gameName);
    playingGames.remove(gameName);
    waitingGames.remove(gameName);
  }

  public void removePlayer(UUID uuid) {
    System.out.println("Dropped client: " + connectedClients.get(uuid));
    connectedClients.remove(uuid);
  }

  public static void main(String[] args) {
    System.setProperty("java.rmi.server.hostname", "192.168.0.1");
    try {
      new Server();
    } catch (RemoteException | AlreadyBoundException e) {
      e.printStackTrace();
    }
  }
}
