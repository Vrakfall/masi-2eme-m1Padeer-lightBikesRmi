import java.awt.*;

/**
 * This is the AI abstract class, shared by all IAs, which has only one method to be called to decide a new direction.
 *
 * @author Sam, Jérémy Lecocq
 */
public abstract class AI extends Player {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public AI(String nickname, Color color, int xPosition, int yPosition, boolean isHuman, char carDirection) {
    super(nickname, color, xPosition, yPosition, isHuman, carDirection);
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  /**
   * Sets a new direction for the AI based on the environment and the AI difficulty.
   *
   * @param grid  a copy of the current grid representation
   * @param timer a copy of the current freshness information
   */
  public abstract void setNewDir(Color[][] grid, Integer[][] timer);

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  /**
   * Convert the car direction into its corresponding integer value.
   *
   * @return The corresponding int value. If the input is invalid, returns 0 ("Up");
   */
  protected int getCarDirectionInt() {
    switch (getCarDirection()) {
      case 'U':
        return 0;
      case 'R':
        return 1;
      case 'D':
        return 2;
      case 'L':
        return 3;
    }

    return 0;
  }

  /**
   * Return the target tile's element based on the player's direction in integer.
   *
   * @param tiles The table of elements to heandle.
   * @return The target tile's color.
   */
  protected <E> E getTargetTileElement(E[][] tiles, int direction) {
//    System.out.println("Direction: " + direction);
//    System.out.println("X: " + getPositionX());
//    System.out.println("Y: " + getPositionY());
    //TODO Remove debug info

    switch (direction) {
      case 0:
        return tiles[getPositionX()][getPositionY() - 1];
      case 1:
        return tiles[getPositionX() + 1][getPositionY()];
      case 2:
        return tiles[getPositionX()][getPositionY() + 1];
      case 3:
        return tiles[getPositionX() - 1][getPositionY()];
    }
    return tiles[getPositionX()][getPositionY()];
  }

  //======== Private-Package ========//

  //======== Private ========//

}
